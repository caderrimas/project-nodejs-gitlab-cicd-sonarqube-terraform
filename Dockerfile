FROM node:20.11-slim AS build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

FROM node:20.11-slim 

WORKDIR /app

COPY --from=build /app/ .

EXPOSE 9000

CMD ["npm", "start"]