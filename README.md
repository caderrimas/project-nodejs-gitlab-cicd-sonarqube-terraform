## Project Nodejs Application Using Gitlab CICD, SonarQube and Terraform 

### Architecture Diagram 

  <div align="center">
  <p>
    ![image](/uploads/4b29c9598058c0609cbf9f8bb5ebd73f/image.png)
  </p>
</div>


1. Directory Structure
     
        |----Terraform/
        |----Dockerfile
        |----sonar-project.properties
        |----.gitlab-ci.yml
        |----package.json
        |----package-lock.json
        |----README.md
        |----index.js
        |----node_modules/
        

2. Check Whether your NodeJS Application is Running in your Local or Not ...

      |---> use "**npm start**" to start the Application  

  ![image](/uploads/e907f8218b9fc46b4e7199236028cb16/image.png)

  ![image](/uploads/93e231343a01a360d6c68c8295ee19c7/image.png)

## Dockerization: Dockerize the NodeJS Application
3. Dockerfile is a simple way to automate the process of building images 
```
FROM node:20.11-slim AS build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

FROM node:20.11-slim 

WORKDIR /app

COPY --from=build /app/ .

EXPOSE 9000

CMD ["npm", "start"]
```

## CICD: Continous Integration & Continuous Deployment Using Gitlab CI
4. Gitlab CICD 

    |---> Create "**.gitlab-ci.yml**" File to Start the CICD Process

    |---> In "**.gitlab-ci.yml**" File We are Having Three Stages

        |---> **Build Stage**  : This stage will build the Dockerfile and get the image and push to AWS ECR Registry
        |---> **Test Stage**   : We are using SonarQube for Code Quality it will check the security vulnerabilities
        |---> **Deploy Stage** : This stage will pull the image from ECR and deploy the image in EC2

```
stages:
  - build
  - test
  - deploy

variables:
  AWS_DEFAULT_REGION: "us-east-1"
  AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
  AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  ECR_REPO: $ECR_REPO
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ''

.aws_image:
  image:
    name: amazon/aws-cli:2.9.21
    entrypoint: ['']

.build:
  extends: .aws_image
  services:
    - docker:23.0.0-dind
  before_script:
    - amazon-linux-extras install docker
    - aws --version
    - docker --version
    - aws ecr get-login-password | docker login --username AWS --password-stdin $ECR_REPO

build develop:
  stage: build
  extends:
    - .build
  variables:
    ECR_DEVELOP: $ECR_REPO:develop
  script:
    - docker build --tag $ECR_DEVELOP .
    - docker push $ECR_DEVELOP
  only:
    - develop

test sonarqube-check:
  image: 
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner
  allow_failure: true

deploy develop:
  stage: deploy
  script: 
    - mkdir -p ~/.ssh
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval "$(ssh-agent -s)"
    - ssh-add ~/.ssh/id_rsa

    - ssh -i ~/.ssh/new_id_rsa -o StrictHostKeyChecking=no $USER@$EC2_INSTANCE_IP "docker stop demo-myapp || true && docker rm demo-myapp || true"
    - ssh -i ~/.ssh/new_id_rsa -o StrictHostKeyChecking=no $USER@$EC2_INSTANCE_IP "aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin $ECR_REPOSITORY_URL"
    - ssh -i ~/.ssh/new_id_rsa -o StrictHostKeyChecking=no $USER@$EC2_INSTANCE_IP "docker pull $ECR_REPO:develop"
    - ssh -i ~/.ssh/new_id_rsa -o StrictHostKeyChecking=no $USER@$EC2_INSTANCE_IP "docker run -d -p 9001:9000 --name=demo-myapp $ECR_REPO:develop"
```
*** SonarQube Quality Gate ***
    
    |---> Once Pipelines Got Succeeded it will give you the SonarQube Quality Gate Urls

![image](/uploads/665e818cb29242f778822bf36facceaa/image.png)

*** Final Output: ***

![image](/uploads/b407f03423015beb658cebf950edf303/image.png)